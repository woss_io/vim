"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => General
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Sets how many lines of history VIM has to remember
set history=1000
set undolevels=1000
set number
set ignorecase
set smartcase
set incsearch
set nolazyredraw
set magic
set showmatch
"change search functions instead of grep use ack-grep
set grepprg=ack-grep\ -a
set guioptions+=b 
" Enable filetype plugin
filetype plugin on
filetype indent on

" Set to auto read when a file is changed from the outside
set autoread
"this is very usefull when u have multiple machines
let hostname = substitute(system('hostname'), '\n', '', '')
let system_uname = system("uname")

" With a map leader it's possible to do extra key combinations
let mapleader = ","
let g:mapleader = ","

" The PC is fast enough, do syntax highlight syncing from start
autocmd BufEnter * :syntax sync fromstart


" When vimrc is edited, reload it
autocmd! bufwritepost vimrc source ~/.vim/.vimrc

" No sound on errors
set noerrorbells
set novisualbell
set t_vb=
set tm=500

call pathogen#runtime_append_all_bundles()
call pathogen#helptags()

autocmd FileType html,htmldjango,jinjahtml,eruby,mako let b:closetag_html_style=1

autocmd FileType html,xhtml,xml,htmldjango,jinjahtml,eruby,mako source ~/.vim/bundle/closetag/plugin/closetag.vim
let g:tagbar_usearrows = 1

"for snoobi workstation
if hostname == 'hell' 
	nnoremap <leader>l :TagbarToggle<CR>
else
	nnoremap <leader>l :TagbarToggle<CR>
endif
map q :NERDTreeToggle<CR>

"configuring TAGLIST file
let Tlist_Ctags_Cmd = "/usr/bin/ctags"
let Tlist_WinWidth = 50
map <F4> :TlistToggle<cr>
"Search and destroy using tags
map <F8> :!/usr/bin/ctags -R --c++-kinds=+p --fields=+iaS --extra=+q /home/danijel/snoobidev/git/<CR>
autocmd FocusGained * let @z=@+

"nmap <silent> <leader>rr :call ReloadSnippets(snippets_dir, &filetype)<CR>
nmap <silent> <leader>ev :e $MYVIMRC<CR>
nmap <silent> <leader>rv :so $MYVIMRC<CR>

"Set red background for text that goes beyond 120 cols
augroup vimrc_autocmds
  au!
  autocmd BufRead * highlight OverLength ctermbg=red ctermfg=white guibg=#592929
  autocmd BufRead * match OverLength /\%120v.*/
augroup END

" this is for assigning .class as .php file for syntax highlighter
au BufNewFile,BufRead *.class set filetype=php
au BufNewFile,BufRead *.inc set filetype=php
au BufRead,BufNewFile *.json set filetype=json


nmap <silent> <leader>js :se ft=javascript<CR>
nmap <silent> <leader>hw :se ft=htmltwig<CR>

map <F9> :set noet|retab!<CR> 

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => PHP documentator
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
source ~/.vim/bundle/phpdoc/plugin/php-doc.vim
inoremap <C-P> <ESC>:call PhpDocSingle()<CR>i
nnoremap <C-P> :call PhpDocSingle()<CR>
vnoremap <C-P> :call PhpDocRange()<CR>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Text, tab and indent related, folding
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set shiftwidth=3
set tabstop=3
set smarttab

set lbr
set tw=500
inoremap <C-Space> <C-n>
set ai "Auto indent
set si "Smart indet
set wrap "Wrap lines

"general mapping for usability
nmap <silent> <leader>s :split<CR>
nmap <silent> <leader>vs :vsplit<CR>
nmap <silent> <leader>q :quit<CR>
nmap <silent> <leader>x :x<CR>
nmap <silent> <leader>w :w<CR>

"PHP integration Vim
let g:DisableAutoPHPFolding = 1 
map <F5> <Esc>:EnableFastPHPFolds<Cr> 
map <F6> <Esc>:DisablePHPFolds<Cr>
map <F7> <Esc>:EnablePHPFolds<Cr> 

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Colors and Fonts
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
syntax enable "Enable syntax hl

" Set font according to system
	if has('unix')
		if system_uname == 'Darwin\n'
			set gfn=Menlo:h16
			set shell=/bin/bash
		else
			set guifont=Courier\ 10\ Pitch\ 10
			set shell=/bin/bash
		endif
	elseif has("win32")
		set gfn=Bitstream\ Vera\ Sans\ Mono:h14
	else
		set guifont=Courier\ 10\ Pitch\ 10
	endif

if has("gui_running")
	set guioptions-=T
	set t_Co=256
	set background=dark
	set lines=58 columns=139
	colorscheme twilight
	"colorscheme zellner
	"colorscheme sweyla
else
	colorscheme zellner
	set background=dark
endif

set encoding=utf8
try
	 lang en_US
catch
endtry

set ffs=unix,dos,mac "Default file types

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Files, backups and undo
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Turn backup off, since most stuff is in SVN, git anyway...
set nobackup
set nowb
set noswapfile

"Persistent undo
try
	 if has("windows")
		set undodir=C:\Windows\Temp
	 else
		set undodir=~/.vim_runtime/undodir
	 endif
		set undofile
catch
endtry

""""""""""""""""""""""""""""""
" => Visual mode related
""""""""""""""""""""""""""""""
" Really useful!
"	In visual mode when you press * or # to search for the current selection
vnoremap <silent> * :call VisualSearch('f')<CR>
vnoremap <silent> # :call VisualSearch('b')<CR>

" When you press gv you vimgrep after the selected text
vnoremap <silent> gv :call VisualSearch('gv')<CR>
map <leader>g :vimgrep // **/*.<left><left><left><left><left><left><left>


function! CmdLine(str)
	 exe "menu Foo.Bar :" . a:str
	 emenu Foo.Bar
	 unmenu Foo
endfunction 

" From an idea by Michael Naumann
function! VisualSearch(direction) range
	 let l:saved_reg = @"
	 execute "normal! vgvy"

	 let l:pattern = escape(@", '\\/.*$^~[]')
	 let l:pattern = substitute(l:pattern, "\n$", "", "")

	 if a:direction == 'b'
		  execute "normal ?" . l:pattern . "^M"
	 elseif a:direction == 'gv'
		  call CmdLine("vimgrep " . '/'. l:pattern . '/' . ' **/*.')
	 elseif a:direction == 'f'
		  execute "normal /" . l:pattern . "^M"
	 endif

	 let @/ = l:pattern
	 let @" = l:saved_reg
endfunction

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => buffers
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Close the current buffer
map <leader>bd :Bclose<cr>
 
" Close all the buffers
map <leader>ba :1,300 bd!<cr>

" Use the arrows to something usefull
"map <right> :bn<cr>
"map <left> :bp<cr>

command! Bclose call <SID>BufcloseCloseIt()
function! <SID>BufcloseCloseIt()
	let l:currentBufNum = bufnr("%")
	let l:alternateBufNum = bufnr("#")

	if buflisted(l:alternateBufNum)
	  buffer #
	else
	  bnext
	endif

	if bufnr("%") == l:currentBufNum
	  new
	endif

	if buflisted(l:currentBufNum)
	  execute("bdelete! ".l:currentBufNum)
	endif
endfunction

" Specify the behavior when switching between buffers 
try
  set switchbuf=usetab
  set stal=2
catch
endtry



"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Moving around, tabs
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Map space to / (search) and c-space to ? (backgwards search)
map <space> /
map <a-space> ?
map <silent> <leader><cr> :noh<cr>


" Tab configuration
map <leader>tn :tabnew<cr>
map <leader>te :tabedit 
map <leader>tc :tabclose<cr>
map <leader>tm :tabmove 

" When pressing <leader>cd switch to the directory of the open buffer
map <leader>cd :cd %:p:h<cr>

" Indents to the right
vmap <C-x> >gv

"indents to the left
vmap <C-z> <gv

"we don't need to have classic copy/paste for mac, oly for linux

if hostname == 'hell'
	
	noremap <C-A> gggH<C-O>G
	inoremap <C-A> <C-O>gg<C-O>gH<C-O>G
	cnoremap <C-A> <C-C>gggH<C-O>G
	onoremap <C-A> <C-C>gggH<C-O>G
	snoremap <C-A> <C-C>gggH<C-O>G
	xnoremap <C-A> <C-C>ggVG

	"copy
	vmap <A-c> "+ygv"zy`>

	"paste
	nmap <A-v> "zgP
	nmap <A-v> "zgp
	imap <A-v> <C-r><C-o>z
	vmap <A-v> "zp`]
	cmap <F1> <C-r><C-o>z

endif
""""""""""""""""""""""""""""""
" => Statusline
""""""""""""""""""""""""""""""
" Always hide the statusline
set laststatus=2

" Format the statusline
if hostname == 'hell'
	let curdir =  substitute(getcwd(), '/home/danijel/snoobidev/', "~/", "g")
else
	let curdir = substitute(getcwd(), '/Users/danijel/', "~/", "g")
endif 

set statusline=\ %{HasPaste()}%F%m%r%h\ %w\ \ CWD:\ %r%{curdir}%h\ \ \ Line:\ %l/%L:%c

function! HasPaste()
	 if &paste
		  return 'PASTE MODE  '
	 else
		  return ''
	 endif
endfunction

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => General Abbrevs
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
iab xdate <c-r>=strftime("%d/%m/%y %H:%M:%S")<cr>


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Cope
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Do :help cope if you are unsure what cope is. It's super useful!
map <leader>cc :botright cope<cr>
map <leader>n :cn<cr>
map <leader>p :cp<cr>


""""""""""""""""""""""""""""""
" => bufExplorer plugin
""""""""""""""""""""""""""""""
let g:bufExplorerDefaultHelp=0
let g:bufExplorerShowRelativePath=1
map <leader>o :BufExplorer<cr>


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Omni complete functions
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
autocmd FileType css set omnifunc=csscomplete#CompleteCSS

""""""""""""""""""""""""""""""
" => JavaScript section
"""""""""""""""""""""""""""""""
map <leader>jt  <Esc>:%!json_xs -f json -t json-pretty<CR>

""""""""""""""""""""""""""""""
" => Command-T
""""""""""""""""""""""""""""""
let g:CommandTMaxHeight = 15
set wildignore+=*.o,*.obj,.git,*.pyc,*.meta,*.log,.git,*.map,*.db,*.cache.php,cache/*

noremap <leader>j :CommandT<cr>
noremap <leader>y :CommandTFlush<cr>

if has( "unix" )
	if hostname == "hell"
		nmap <silent> <leader>r :CommandT /home/danijel/snoobidev/git/restapi<CR>
		nmap <silent> <leader>c :CommandT /home/danijel/snoobidev/git/coreIncludes<CR>
		nmap <silent> <leader>re :CommandT /home/danijel/snoobidev/git/reports<CR>
		nmap <silent> <leader>e :CommandT /home/danijel/snoobidev/git/erp<CR>
		nmap <silent> <leader>i :CommandT /home/danijel/snoobidev/git/integrationcenter<CR>
		nmap <silent> <leader>ma :CommandT /home/danijel/snoobidev/git/mailer<CR>
	endif
elseif has("mac")
	nmap <silent> <leader>r :CommandT /Users/danijel/Workspaces<CR>
	
endif

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => MISC
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Remove the Windows ^M - when the encodings gets messed up
noremap <Leader>m mmHmt:%s/<C-V><cr>//ge<cr>'tzt'm

"Quickly open a buffer for scripbble
map <leader>q :e ~/buffer<cr>
au BufRead,BufNewFile ~/buffer iab <buffer> xh1 ===========================================

map <leader>pp :setlocal paste!<cr>

map <leader>bb :cd ..<cr>

"this uses xmllint and reformat partialy non indented files....
"http://vim.wikia.com/wiki/Format_your_xml_document_using_xmllint
au FileType xml exe ":silent 1,$!xmllint --format --recover - 2>/dev/null"
